package inno.homeworks.homework11;

import inno.homeworks.intermediatetest.FileReadingException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.List;


public class CarsRepositoryFileBasedImpl implements CarsRepository {
    private final String repositoryFilePath;
    private final String fileReadingExceptionMessage;

    public CarsRepositoryFileBasedImpl(String filePath) {
        repositoryFilePath = filePath;
        fileReadingExceptionMessage = "Unsuccessful attempt to read from file: " + repositoryFilePath;
    }

    private Car stringToCar(String string) {
        String[] carDetails = string.split("\\|");

        String carNumber = carDetails[0];
        String carModel = carDetails[1];
        String carColor = carDetails[2];
        int carMileage = Integer.parseInt(carDetails[3]);
        int carPrice = Integer.parseInt(carDetails[4]);

        return new Car(carNumber, carModel, carColor, carMileage, carPrice);
    }

    @Override
    List<String> getBlackOrZeroMileageCarNumbers() {
         try (FileReader carsFile = new FileReader(repositoryFilePath);
                BufferedReader carsRepository = new BufferedReader(carsFile)) {
               return carsRepository
                       .lines()
                       .map(this::stringToCar)
                       .filter(car -> car.getCarColor().equalsIgnoreCase("black") || car.getCarMileage() == 0)
                       .map(Car::getCarNumber)
                       .toList();
           } catch (IOException e) {
               throw new FileReadingException("Unsuccessful attempt to read from file: " + repositoryFilePath, e);
           }
    }

    @Override
    public String getCheapestCarColor() {

        try (FileReader carsFile = new FileReader(repositoryFilePath);
             BufferedReader carsRepository = new BufferedReader(carsFile)) {
            return carsRepository
                    .lines()
                    .map(this::stringToCar)
                    .min(Comparator.comparingInt(Car::getCarPrice))
                    .orElseThrow(NoSuchElementException::new)
                    .getCarColor();
        } catch (IOException e) {
            throw new FileReadingException(fileReadingExceptionMessage, e);
        }
    }

    @Override
    public double getAveragePriceByModel(String model) {

        try (FileReader carsFile = new FileReader(repositoryFilePath);
             BufferedReader carsRepository = new BufferedReader(carsFile)) {
            return carsRepository
                    .lines()
                    .map(this::stringToCar)
                    .filter(car -> car.getCarModel().equalsIgnoreCase(model))
                    .mapToDouble(Car::getCarPrice)
                    .average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new FileReadingException(fileReadingExceptionMessage, e);
        }
    }
    
    @Override
    public long getUniqueCarCountInPriceRange(int from, int to) {

        try (FileReader carsFile = new FileReader(repositoryFilePath);
             BufferedReader carsRepository = new BufferedReader(carsFile)) {
            return carsRepository
                    .lines()
                    .map(this::stringToCar)
                    .filter(car -> car.getCarPrice() >= from && car.getCarPrice() <= to)
                    .map(Car::getCarModel)
                    .distinct()
                    .count();
        } catch (IOException e) {
            throw new FileReadingException(fileReadingExceptionMessage, e);
        }
    }
}