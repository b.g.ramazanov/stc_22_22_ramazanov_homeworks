package inno.homeworks.homework11;

import java.util.List;

public interface CarsRepository {
    long getUniqueCarCountInPriceRange(int from, int to);

    String getCheapestCarColor();

    double getAveragePriceByModel(String model);
    
    List<String> getBlackOrZeroMileageCarNumbers();
}