package inno.homeworks.homework11;

import java.util.Objects;

public class Car {
    private final String carColor;
    private final String carNumber;
    private final String carModel;
    private final int carMileage;
    private final int carPrice;

    public Car(String number, String model, String color, int mileage, int price) {
        carModel = model;
        carColor = color;
        carMileage = mileage;
        carPrice = price;
        carNumber = number;
    }

    public String getCarColor() {
        return carColor;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public String getCarModel() {
        return carModel;
    }

    public int getCarMileage() {
        return carMileage;
    }

    public int getCarPrice() {
        return carPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car car)) return false;
        return carMileage == car.carMileage && carPrice == car.carPrice && carColor.equals(car.carColor) && carNumber.equals(car.carNumber) && carModel.equals(car.carModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carColor, carNumber, carModel, carMileage, carPrice);
    }
}