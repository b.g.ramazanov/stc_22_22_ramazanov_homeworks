package inno.homeworks.homework11;

// import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        CarsRepositoryFileBasedImpl cars = new CarsRepositoryFileBasedImpl("resources/repository.txt");

        System.out.println(cars.getCheapestCarColor());
        System.out.println(cars.getUniqueCarCountInPriceRange(60000, 70000));        
        System.out.println(cars.getBlackOrZeroMileageCarNumbers());
        System.out.println(cars.getAveragePriceByModel());
    }
}
