package inno.homeworks.intermediatetest;

public class Main {
    public static void main(String[] args) {
        ProductsRepositoryFileBasedImpl products = new ProductsRepositoryFileBasedImpl("resources/products.txt");

//        products.update(new Product(15, "Кокос", 30, 15));

        System.out.println(products.findById(1));
        System.out.println(products.findById(11));

//        for (Product prod : products.findAllByTitleLike("око")) {
//            System.out.println(prod);
//        }

        for (Product prod : products.findAllByTitleLike("око")) {
            System.out.println(prod);
        }
    }
}
