package inno.homeworks.intermediatetest;

import java.util.Objects;

public class Product {
    private int productQuantity;
    private double productPrice;
    private String productTitle;
    private final int id;

    public Product(int id, String productTitle, double productPrice, int productQuantity) {
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
        this.productTitle = productTitle;
        this.id = id;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductName(String productTitle) {
        this.productTitle = productTitle;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product product)) return false;
        return id == product.id && productTitle.equals(product.productTitle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productTitle, id);
    }

    @Override
    public String toString() {
        return id + "|" + productTitle + "|" + productPrice + "|" + productQuantity;
    }
}