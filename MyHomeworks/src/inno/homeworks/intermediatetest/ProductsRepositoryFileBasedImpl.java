package inno.homeworks.intermediatetest;

import java.io.*;

import java.util.ArrayList;
import java.util.List;


public class ProductsRepositoryFileBasedImpl implements ProductsRepository {
    private final String repositoryFilePath;
    private final String fileReadingExceptionMessage;

    public ProductsRepositoryFileBasedImpl(String filePath) {
        repositoryFilePath = filePath;
        fileReadingExceptionMessage = "Unsuccessful attempt to read from file: " + repositoryFilePath;
    }

    private Product stringToProduct(String string) {
        String[] productDetails = string.split("\\|");

        return arrayToProduct(productDetails);
    }

    private Product arrayToProduct(String[] productDetails) {
        int productId = Integer.parseInt(productDetails[0]);
        String productTitle = productDetails[1];
        double productPrice = Double.parseDouble(productDetails[2]);
        int productQuantity = Integer.parseInt(productDetails[3]);

        return new Product(productId, productTitle, productPrice, productQuantity);
    }

    @Override
    public Product findById(Integer id) {
        try (FileReader productsFile = new FileReader(repositoryFilePath);
             BufferedReader productsRepository = new BufferedReader(productsFile)) {

            String product;
            String productId = id + "|";

            while ((product = productsRepository.readLine()) != null) {
                if (product.startsWith(productId)) {
                    return stringToProduct(product);
                }
            }

            return null;

        } catch (IOException e) {
            throw new FileReadingException(fileReadingExceptionMessage, e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (FileReader productsFile = new FileReader(repositoryFilePath);
             BufferedReader productsRepository = new BufferedReader(productsFile)) {

            String productLine;
            List<Product> products = new ArrayList<>();

            while ((productLine = productsRepository.readLine()) != null && !productLine.equals("")) {
                String[] productDetails = productLine.split("\\|");

                if (productDetails[1].toLowerCase().contains(title.toLowerCase())) {
                    Product product = arrayToProduct(productDetails);
                    products.add(product);
                }
            }

            return products;

        } catch (IOException e) {
            throw new FileReadingException(fileReadingExceptionMessage, e);
        }
    }

    public List<Product> getAll() {
        try (FileReader productsFile = new FileReader(repositoryFilePath);
             BufferedReader productsRepository = new BufferedReader(productsFile)) {

            return productsRepository
                    .lines()
                    .map(this::stringToProduct)
                    .toList();

        } catch (IOException e) {
            throw new FileReadingException(fileReadingExceptionMessage, e);
        }
    }

    @Override
    public void update(Product product) {
        List<String> products;
        String id = product.getId() + "|";

        try (FileReader productsFile = new FileReader(repositoryFilePath);
             BufferedReader productsRepository = new BufferedReader(productsFile)) {

            products = productsRepository
                    .lines()
                    .map(it -> {
                        if (it.startsWith(id)) {
                            return product.toString();
                        } else {
                            return it;
                        }
                    })
                    .toList();

        } catch (IOException e) {
            throw new FileReadingException(fileReadingExceptionMessage, e);
        }

        try (FileWriter writer = new FileWriter(repositoryFilePath);
             BufferedWriter fileWriter = new BufferedWriter(writer)) {
            for (String p : products) {
                fileWriter.write(p + "\n");
            }

        } catch (IOException e) {
            throw new FileWritingException("Unsuccessful attempt to write in file: " + repositoryFilePath, e);
        }
    }
}