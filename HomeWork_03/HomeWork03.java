import java.util.Scanner;

public class HomeWork03 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int min_count = 0;

        System.out.println("Введите размер массива: ");
        int arr_size = scanner.nextInt();

        if (arr_size == 1) {
            System.out.println("Количество локальных минимумов: 1");
            return;
        }

        int[] arr = new int[arr_size];

        for (int i = 0; i < arr_size; i++) {
            System.out.println("Введите число: ");
            arr[i] = scanner.nextInt();
        }

        for (int i = 0; i < arr_size; i++) {
            if (i == 0) {
                if (arr[i] < arr[i + 1]) min_count++;
            } else if (i == arr_size - 1) {
                if (arr[i] < arr[i - 1]) min_count++;
            } else {
                if (arr[i] < arr[i + 1] && arr[i] < arr[i - 1]) min_count++;
            }
        }

        System.out.println("Количество локальных минимумов: " + min_count);

    }
}