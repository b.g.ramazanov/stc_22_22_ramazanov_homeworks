public class Main {
    public static void main(String[] args) {
        ArrayTask intervalSum = (arr, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) sum += arr[i];
            return sum;
        };

        ArrayTask maxNumDigitSum = (arr, from, to) -> {
            int sum = 0;
            int max = arr[from];

            for (int i = from + 1; i <= to; i++) if (arr[i] > max) max = arr[i];
            for (; max > 0; max /= 10) sum += max % 10;

            return sum;
        };

        int[] numbers = {12, 629, 47, 2, 100, 40, 56, 250};

        ArraysTasksResolver.resolveTask(numbers, intervalSum, 1, 5);

        System.out.println();

        ArraysTasksResolver.resolveTask(numbers, maxNumDigitSum, 0, 6);
    }
}