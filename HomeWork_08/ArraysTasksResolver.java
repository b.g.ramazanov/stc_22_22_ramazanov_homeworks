import java.util.Arrays;

public class ArraysTasksResolver {
    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        if (from > to) throw new RuntimeException("The value of \'from\' can\'t be greater than the value of \'to\'");

        System.out.println("Original array:");
        System.out.println(Arrays.toString(array));

        System.out.println("Elements of array from " + from + " to " + to + ":");
        for (int i = from; i <= to; i++) {
            System.out.print(array[i] + " ");
        }

        System.out.println();

        System.out.println("The result of the ArrayTask application:");
        System.out.println(task.resolve(array, from, to));
    }
}