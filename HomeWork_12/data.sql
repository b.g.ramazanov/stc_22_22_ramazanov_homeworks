insert into driver (firstname, lastname, experience, phone_number, have_driver_license, driver_category, rank)
values ('Михаил', 'Мартынов', 4,  '+7 (998) 375-11-68', true, 'A, B',    2),
       ('Андрей', 'Яковлев',  7,  '+7 (958) 205-42-62', true, 'B',       3),
       ('Даниил', 'Никулин',  12, '+7 (981) 954-42-28', true, 'B, C',    4),
       ('Пётр',   'Морозов',  8,  '+7 (982) 823-86-41', true, 'B, C',    1),
       ('Руслан', 'Беляев',   19, '+7 (911) 848-46-32', true, 'B, C, D', 4),
       ('Никита', 'Новиков',  10, '+7 (931) 395-58-55', true, 'B, C',    2);

insert into car (brand, model, car_color, car_number, driver_id)
values ('Citroen', 'C3 Picasso', 'Тёмно-серый', 'М607НВ60', 1),
       ('Honda',   'Civic',      'Серо-синий',  'Н492ХА80', 2),
       ('Ford',    'Fusion',     'Серебристый', 'К092ХР23', 2),
       ('BMW',     'X4',         'Черный',      'Е118АТ51', 3),
       ('Hyundai', 'Creta',      'Белый',       'С629ЕТ78', 6);

insert into trip (driver_id, car_id, trip_date, trip_duration)
values (1, 1, '2021-12-07', '17 days'),
       (3, 4, '2022-04-12', '19 days'),
       (4, 3, '2021-07-13', '15 days'),
       (2, 3, '2022-01-07', '20 days'),
       (2, 2, '2021-12-22', '23 days');