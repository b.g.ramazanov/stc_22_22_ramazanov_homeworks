create table driver (
    id                  bigserial    primary key,
    firstname           varchar(20)  not null,
    lastname            varchar(20)  not null,
    experience          int          default 0,
    phone_number        varchar(20)  unique,
    have_driver_license bool,
    driver_category     varchar(20),
    rank                smallint     check (rank >= 0 and rank <= 5)
);

create table car (
    id         bigserial    primary key,
    brand      varchar(20)  not null,
    model      varchar(20)  not null,
    car_color  varchar(30),
    car_number varchar(10)  unique not null,
    driver_id  bigint       references driver(id)
);

create table trip (
    id            bigserial primary key,
    driver_id     bigint    references driver(id) not null,
    car_id        bigint    references car(id) not null,
    trip_date     date,
    trip_duration interval
);