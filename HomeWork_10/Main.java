import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a string: ");
        String text = scanner.nextLine();
        String[] words = {};

        // split для пустой строки возвращает массив с 1 элементом - пустой строкой.
        // исключаем ее
        if (!text.equals("")) {
            words = text.split(" ");
        }

        Map<String, Integer> wordCounter = new HashMap<>();

        for (String word : words) {
            wordCounter.putIfAbsent(word, 0);
            wordCounter.compute(word, (key, val) -> val + 1);
        }

        String mostFrequentWord = null;
        Integer maxCount = 0;

        for (Map.Entry<String, Integer> wordStat : wordCounter.entrySet()) {
            if (wordStat.getValue() > maxCount) {
                mostFrequentWord = wordStat.getKey();
                maxCount = wordStat.getValue();
            }
        }

        if (mostFrequentWord != null) {
            System.out.println(mostFrequentWord + " " + maxCount);
        } else {
            System.out.println("No words were found");
        }

        scanner.close();
    }
}