public class Main {
    public static void main(String[] args) {
        Task[] tasks = new Task[6];

        // tasks[0] = new EvenNumbersPrintTask(0, 10);
        tasks[0] = new EvenNumbersPrintTask(10, 0);
        tasks[1] = new OddNumbersPrintTask(5, 20);
        tasks[2] = new EvenNumbersPrintTask(-7, 0);
        tasks[3] = new OddNumbersPrintTask(100, 115);
        tasks[4] = new EvenNumbersPrintTask(56, 70);
        tasks[5] = new OddNumbersPrintTask(12, 17);

        completeAllTasks(tasks);
    }

    public static void completeAllTasks(Task[] tasks) {
        for(Task task : tasks) {
            task.complete();
            System.out.println();
        }
    }
}