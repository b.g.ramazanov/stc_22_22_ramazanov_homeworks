public abstract class AbstractNumbersPrintTask implements Task {
    private int from;
    private int to;

    public AbstractNumbersPrintTask(int lowerBound, int upperBound) {
        if(lowerBound > upperBound) {
            throw new RuntimeException("Lower bound must be less than or equal to upper bound");
        }

        from = lowerBound;
        to = upperBound;
    }

    public int getLowerBound() {
        return from;
    }

    public int getUpperBound() {
        return to;
    }

    @Override
    public abstract void complete();
}