public class OddNumbersPrintTask extends AbstractNumbersPrintTask {
    public OddNumbersPrintTask(int lowerBound, int upperBound) {
        super(lowerBound, upperBound);
    }

    @Override
    public void complete() {
        for(int i = getLowerBound(); i < getUpperBound(); i++) {
            if(i % 2 != 0) {
                System.out.println(i);
            }            
        }        
    }
}