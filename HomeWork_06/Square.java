public class Square extends Rectangle {
    public Square(double x, double y, double side) {
        super(x, y, side, side);
    }

    public double getSide() {
        return getFirstSide();
    }

    public void setSide(double side) {
        setFirstSide(side);
    }

    @Override
    public void setFirstSide(double side) {
        super.setFirstSide(side);
        super.setSecondSide(side);
    }

    @Override
    public void setSecondSide(double side) {
        setFirstSide(side);
    }
}