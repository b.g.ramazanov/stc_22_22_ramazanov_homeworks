public class Rectangle extends Figure {
    private double firstSide;
    private double secondSide;

    public Rectangle(double x, double y, double first, double second) {
        super(x, y);

        if (firstSide < 0 || secondSide < 0) {
            throw new RuntimeException("Side cannot be less than 0");
        }

        firstSide = first;
        secondSide = second;
    }

    public double getFirstSide() {
        return firstSide;
    }

    public void setFirstSide(double side) {
        if (side < 0) throw new RuntimeException("Side cannot be less than 0");

        firstSide = side;
    }

    public double getSecondSide() {
        return secondSide;
    }

    public void setSecondSide(double side) {
        if (side < 0) throw new RuntimeException("Side cannot be less than 0");

        secondSide = side;
    }

    @Override
    public double perimeter() {
        return 2 * (firstSide + secondSide);
    }

    @Override
    public double square() {
        return firstSide * secondSide;
    }
}