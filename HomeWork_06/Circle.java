public class Circle extends Ellipse {
    public Circle(double x, double y, double radius) {
        super(x, y, radius, radius);
    }

    public double getRadius() {
        return getFirstRadius();
    }

    public void setRadius(double radius) {
        setFirstRadius(radius);
    }

    @Override
    public void setFirstRadius(double radius) {
        super.setFirstRadius(radius);
        super.setSecondRadius(radius);
    }

    @Override
    public void setSecondRadius(double radius) {
        setFirstRadius(radius);
    }
}