public class Main {
    public static void main(String[] args) {
        Circle a = new Circle(5, 5, 10);
        Square b = new Square(7, 10, 20);

        a.setRadius(15);
        b.setSide(10);

        System.out.println(a.getRadius());
        System.out.println(b.getSide());
    }
}