public class Ellipse extends Figure {
    private double firstRadius;
    private double secondRadius;


    public Ellipse(double x, double y, double first, double second) {
        super(x, y);

        if (firstRadius < 0 || secondRadius < 0) {
            throw new RuntimeException("Radius cannot be less than 0");
        }

        firstRadius = first;
        secondRadius = second;
    }

    public double getFirstRadius() {
        return firstRadius;
    }

    public void setFirstRadius(double radius) {
        if (radius < 0) throw new RuntimeException("Radius cannot be less than 0");

        firstRadius = radius;
    }

    public double getSecondRadius() {
        return secondRadius;
    }

    public void setSecondRadius(double radius) {
        if (radius < 0) throw new RuntimeException("Radius cannot be less than 0");

        secondRadius = radius;
    }

    @Override
    public double perimeter() {
        return 4 * ((Math.PI * firstRadius * secondRadius + Math.pow((firstRadius - secondRadius), 2)) / 
                    (firstRadius + secondRadius));
    }

    @Override
    public double square() {
        return Math.PI * firstRadius * secondRadius;
    }
}