public abstract class Figure {
    private double x;
    private double y; 

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void move(double toX, double toY) {
        x = toX;
        y = toY;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public abstract double perimeter();

    public abstract double square();
}