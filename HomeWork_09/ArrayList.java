public class ArrayList<T> implements List<T> {

    private final static int DEFAULT_ARRAY_SIZE = 10;

    private T[] elements;

    private int count;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    @Override
    public void add(T element) {
        if (isFull()) {
            resize(count + count / 2);
        }

        elements[count] = element;
        count++;
    }

    private void resize(int newLength) {
        T[] newElements = (T[]) new Object[newLength];

        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }

        this.elements = newElements;
    }

    private boolean isFull() {
        return count == elements.length;
    }

    @Override
    public void remove(T element) {
        int index = indexOf(element);

        if (index != -1) {
            for (int i = index + 1; i < count; i++) {
                elements[i - 1] = elements[i];
            }

            count--;
        }

        if (elements.length - count > count / 2 && count >= DEFAULT_ARRAY_SIZE) {
            resize(count);            
        }
    }

    @Override
    public boolean contains(T element) {
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(element)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {
        if (index >= 0 && index < count) {
            for (int i = index + 1; i < count; i++) {
                elements[i - 1] = elements[i];
            }

            count--;
        }

        if (elements.length - count > count / 2 && count >= DEFAULT_ARRAY_SIZE) {
            resize(count);            
        }
    }

    public int indexOf(T element) {
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(element)) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        }
        return null;
    }
}
