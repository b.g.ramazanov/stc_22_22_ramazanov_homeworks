public interface List<E> extends Collection<E> {
    void removeAt(int index);

    E get(int index);
}
