public class LinkedList<T> implements List<T> {

    private Node<T> first;

    private Node<T> last;
    private int count;

    private static class Node<E> {
        E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }
    }

    @Override
    public void add(T element) {
        Node<T> newNode = new Node<>(element);

        if (count == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }

        this.last = newNode;
        count++;
    }

    @Override
    public void remove(T element) {
        if (count != 0) {
            if (first.value.equals(element)) {
                first = first.next;
                count--;
            } else {
                Node<T> previous = first;

                while (previous.next != null) {
                    if (previous.next.value.equals(element)) {
                        previous.next = previous.next.next;
                        count--;
                        return;
                    }
                    previous = previous.next;
                }
            }     
        }
    }

    @Override
    public boolean contains(T element) {
        Node<T> current = this.first;

        while (current != null) {
            if (current.value.equals(element)) {
                return true;
            }
            current = current.next;
        }

        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {
        if (0 <= index && index < count) {
            if (index == 0) {
                first = first.next;
            } else {
                Node<T> previous = first;

                for (int i = 0; i < index - 1; i++) {
                    previous = previous.next;
                }

                previous.next = previous.next.next;
            }

            count--;
        }
    }

    @Override
    public T get(int index) {
        if (0 <= index && index < count) {
            Node<T> current = this.first;

            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        return null;

    }
}
