import java.util.Random;

public class Main {

    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();

        stringList.remove("Hello!");

        stringList.add("Hello!");
        stringList.add("Bye!");
        stringList.add("Fine!");
        stringList.add("C++!");
        stringList.add("PHP!");
        stringList.add("Cobol!");

        System.out.println(stringList.get(0));
        System.out.println(stringList.get(3));
        System.out.println(stringList.get(5));
        System.out.println(stringList.contains("Fine!"));
        System.out.println(stringList.contains("Cobol!"));
        System.out.println(stringList.contains("Python"));
        System.out.println(stringList.size());

        stringList.remove("Hello!");
        stringList.removeAt(2);
        stringList.removeAt(3);
        stringList.remove("PHP!");
        //stringList.removeAt(0);
        //stringList.removeAt(0);
        //stringList.removeAt(0);
        //stringList.removeAt(0);
        System.out.println(stringList.size());  

        for (int i = 0; i < stringList.size(); i++) {
            System.out.println(stringList.get(i));
        }
    }
}