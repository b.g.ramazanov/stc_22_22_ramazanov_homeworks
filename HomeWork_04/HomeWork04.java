public class HomeWork04 {
    public static void main(String[] args) {

        // int[] number = {9, 8, 7, 2, 3};

        // System.out.println(rangeSum(5, 10));
        // System.out.println(toInt(number));
        // evenArrayItems(number);

    }

    public static int rangeSum(int leftBorder, int rightBorder) {

        if (leftBorder > rightBorder) {
            return -1;
        }

        int sum = 0;

        for (int i = leftBorder; i <= rightBorder; i++) {
            sum += i;
        }

        return sum;
    }

    public static void evenArrayItems(int[] arr) {
        for (int item : arr) {

            if (item % 2 == 0) {
                System.out.println(item);
            }
            
        }
    }

    public static int toInt(int[] arr) {

        int number = 0;

        for (int i = 0; i < arr.length; i++) {
            number += arr[arr.length - 1 - i] * pow(10, i);
        }

        return number;
    }

    public static int pow(int num, int index) {

        int power = 1;

        for (int i = 0; i < index; i++) {
            power *= num;
        }

        return power;
    }
}