package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.models.Book;
import ru.inno.ec.models.Comment;
import ru.inno.ec.repositories.BooksRepository;
import ru.inno.ec.repositories.CommentsRepository;
import ru.inno.ec.services.BooksService;
import java.time.LocalDateTime;
import java.util.List;

@RequiredArgsConstructor
@Service
public class BooksServiceImpl implements BooksService {

    private final CommentsRepository commentsRepository;
    private final BooksRepository booksRepository;

    public void addBook(Book book) {
        book.setPublishDate(LocalDateTime.now());
        booksRepository.save(book);
    }

    @Override
    public Book getBook(Long bookId) {
        return booksRepository.findById(bookId).orElseThrow();
    }

    @Override
    public List<Book> getAllBooks() {
        return booksRepository.findAllByOrderByPublishDateDesc();
    }

    @Override
    public List<Book> getAllBooksByTitle(String query) {
        return booksRepository.findAllByTitleContainsIgnoreCase(query);
    }

    @Override
    public void updateBook(Long bookId, Book updatedBook) {
        Book book = booksRepository.findById(bookId).orElseThrow();

        book.setAuthor(updatedBook.getAuthor());
        book.setPublishDate(LocalDateTime.now());
        book.setIsbn(updatedBook.getIsbn());
        book.setYear(updatedBook.getYear());
        book.setPublisher(updatedBook.getPublisher());
        book.setShortDescription(updatedBook.getShortDescription());
        book.setLongDescription(updatedBook.getLongDescription());

        booksRepository.save(book);
    }
}
