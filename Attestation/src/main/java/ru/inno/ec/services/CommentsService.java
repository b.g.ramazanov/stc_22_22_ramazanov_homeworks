package ru.inno.ec.services;

import ru.inno.ec.models.Book;
import ru.inno.ec.models.Comment;
import ru.inno.ec.models.User;

import java.util.List;

public interface CommentsService {
    List<Comment> getAllComments();
    void deleteComment(Long commentId);
    void addComment(Comment comment, Long bookId, User user);
    List<Comment> getBookComments(Long bookId);
}
