package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.models.Book;
import ru.inno.ec.models.Comment;
import ru.inno.ec.models.User;
import ru.inno.ec.repositories.BooksRepository;
import ru.inno.ec.repositories.CommentsRepository;
import ru.inno.ec.services.CommentsService;

import java.time.LocalDateTime;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CommentsServiceImpl implements CommentsService {

    private final CommentsRepository commentsRepository;
    private final BooksRepository booksRepository;
    @Override
    public List<Comment> getAllComments() {
        return commentsRepository.findAll();
    }

    public void deleteComment(Long commentId) {
        commentsRepository.deleteById(commentId);
    }

    @Override
    public void addComment(Comment comment, Long bookId, User user) {
        Book book = booksRepository.findById(bookId).orElseThrow();
        comment.setPublisher(user.getFirstName() + user.getLastName());
        comment.setBook(book);
        comment.setUser(user);
        comment.setAddingDate(LocalDateTime.now());
        commentsRepository.save(comment);
    }

    public List<Comment> getBookComments(Long bookId) {
        Book book = booksRepository.findById(bookId).orElseThrow();
        return commentsRepository.findAllByBookOrderByAddingDateDesc(book);
    }
}
