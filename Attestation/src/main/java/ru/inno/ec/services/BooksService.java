package ru.inno.ec.services;

import ru.inno.ec.models.Book;
import ru.inno.ec.models.Comment;

import java.util.List;

public interface BooksService {

    Book getBook(Long bookId);
    List<Book> getAllBooks();

    void addBook(Book book);

    void updateBook(Long bookId, Book book);

    List<Book> getAllBooksByTitle(String query);
}
