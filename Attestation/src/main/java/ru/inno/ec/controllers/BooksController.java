package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.ec.models.Book;
import ru.inno.ec.models.User;
import ru.inno.ec.models.Comment;
import ru.inno.ec.security.details.CustomUserDetails;
import ru.inno.ec.services.BooksService;
import ru.inno.ec.services.CommentsService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/books")
public class BooksController {

    private final BooksService booksService;
    private final CommentsService commentsService;

    @GetMapping
    public String getBooksPage(@RequestParam(required = false) String query, @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        User.Role role = customUserDetails == null ? User.Role.USUAL : customUserDetails.getUser().getRole();

        model.addAttribute("role", role);
        if (query != null) {
            model.addAttribute("books", booksService.getAllBooksByTitle(query));
        } else {
            model.addAttribute("books", booksService.getAllBooks());
        }

        return "main_page";
    }

    @GetMapping("/new")
    public String getBookAddPage() {
        return "addbook_page";
    }

    @PostMapping("/new")
    public String addBook(Book book) {
        booksService.addBook(book);
        return "redirect:/books";
    }


    @PostMapping("/{book-id}/comment/add")
    public String addCommentToBook(@PathVariable("book-id") Long bookId, @AuthenticationPrincipal CustomUserDetails customUserDetails, Comment comment) {
        User user = customUserDetails.getUser();
        commentsService.addComment(comment, bookId, user);
        return "redirect:/books/" + bookId;
    }

    @PostMapping("/{book-id}/comment/delete/{comment-id}")
    public String addCommentToBook(@PathVariable("book-id") Long bookId, @PathVariable("comment-id") Long commentId, @AuthenticationPrincipal CustomUserDetails customUserDetails, Comment comment) {

        commentsService.deleteComment(commentId);
        return "redirect:/books/" + bookId;
    }

    @GetMapping("/{book-id}/update")
    public String updateBook(@PathVariable("book-id") Long bookId, Model model) {
        model.addAttribute("book", booksService.getBook(bookId));
        return "updatebook_page";
    }

    @PostMapping("/{book-id}/update")
    public String updateBook(@PathVariable("book-id") Long bookId, Book book) {
        booksService.updateBook(bookId, book);
        return "redirect:/books/" + bookId;
    }

    @GetMapping("/{book-id}")
    public String getBookPage(@PathVariable("book-id") Long bookId, @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        User.Role role = customUserDetails == null ? User.Role.USUAL : customUserDetails.getUser().getRole();

        model.addAttribute("role", role);
        model.addAttribute("book", booksService.getBook(bookId));
        model.addAttribute("comments", commentsService.getBookComments(bookId));
        return "book_page";
    }
}

