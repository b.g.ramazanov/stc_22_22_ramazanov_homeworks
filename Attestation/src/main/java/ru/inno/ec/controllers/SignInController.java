package ru.inno.ec.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SignInController {



    @GetMapping("/")
    public String getRoot() {
        return "redirect:/books";
    }

    @GetMapping("/signIn")
    public String getSignInPage() {
        return "signIn_page";
    }
}
