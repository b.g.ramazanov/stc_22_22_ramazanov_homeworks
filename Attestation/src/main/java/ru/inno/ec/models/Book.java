package ru.inno.ec.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"comments", "longDescription", "shortDescription"})
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(length = 250, name = "short_description")
    private String shortDescription;

    @Column(length = 1000, name = "long_description")
    private String longDescription;
    private Integer year;

    private String isbn;
    private String author;
    private String publisher;
    @Column(name = "publish_date")
    private LocalDateTime publishDate;

    @OneToMany(mappedBy = "book", fetch = FetchType.EAGER)
    private Set<Comment> comments;
}
