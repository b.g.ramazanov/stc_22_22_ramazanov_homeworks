insert into reader (email, password, first_name, last_name, age, role, state)
    values 
    ('name1@mail.ru', 'password1', 'Name1', 'Surname1', 21, 'USER', 'CONFIRMED'),
    ('name2@mail.ru', 'password2', 'Name2', 'Surname2', 22, 'USER', 'CONFIRMED'),
    ('name3@mail.ru', 'password3', 'Name3', 'Surname3', 23, 'USER', 'CONFIRMED'),
    ('name4@mail.ru', 'password4', 'Name4', 'Surname4', 24, 'USER', 'CONFIRMED'),
    ('name5@mail.ru', 'password5', 'Name5', 'Surname5', 25, 'USER', 'CONFIRMED');

insert into book (title, year, author, publisher) 
    values ('Book1', 2013, 'author1', 'publisher1');
    ('Book2', 2013, 'author2', 'publisher2');
    ('Book3', 2013, 'author3', 'publisher3');
    ('Book4', 2013, 'author4', 'publisher4');
    ('Book5', 2013, 'author5', 'publisher5');

insert into book (title, year, author, publisher)
values ('Book1', 2013, 'author1', 'publisher1'),
       ('Book22', 2013, 'author2', 'publisher2'),
       ('Book23', 2013, 'author3', 'publisher3'),
       ('Book26', 2013, 'author4', 'publisher4'),
       ('Book27', 2013, 'author1', 'publisher1'),
       ('Book28', 2013, 'author2', 'publisher2'),
       ('Book29', 2013, 'author3', 'publisher3'),
       ('Book10', 2013, 'author4', 'publisher4'),
       ('Book11', 2013, 'author1', 'publisher1'),
       ('Book12', 2013, 'author2', 'publisher2'),
       ('Book13', 2013, 'author3', 'publisher3'),
       ('Book14', 2013, 'author4', 'publisher4'),
       ('Book15', 2013, 'author5', 'publisher5');