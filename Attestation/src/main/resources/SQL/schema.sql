drop table if exists reader;
drop table if exists book;
drop table if exists comment;

create table book (
    id bigserial primary key,
    title varchar(256),
    short_description varchar(256),
    long_description varchar(1000),
    year integer,
    isbn varchar(64),
    author varchar(128),
    publisher varchar(128),
    publish_date datetime
);

create table comment (
    id bigserial primary key,
    body varchar(1000),
    adding_date datetime
);


create table reader (
    id bigserial primary key,
    email varchar(256) not null,
    password varchar(256) not null,
    first_name varchar(256),
    last_name varchar(256),
    age integer check age >= 0 and age <= 120,
    role varchar(256),
    state varchar(256)
);