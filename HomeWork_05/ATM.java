public class ATM {

    private int currentATMBalance;
    private int cashLimit;
    private int maxATMBalance;
    private int count = 0;

    public ATM(int currentATMBalance, int cashLimit, int maxATMBalance) {
        if(currentATMBalance < 0 || cashLimit < 0 || maxATMBalance < 0) {
            throw new RuntimeException("All parameters must be greater or equal to zero");
        }

        if(currentATMBalance > maxATMBalance || cashLimit > maxATMBalance) {
            throw new RuntimeException("Current balance and max issuance must be less than or equal to max ATM balance");
        }

        this.currentATMBalance = currentATMBalance;
        this.cashLimit = cashLimit;
        this.maxATMBalance = maxATMBalance;
    }

    public int getMoney(int sum) {
        int minSum = Math.min(currentATMBalance, cashLimit);

        if(sum > minSum) {
            sum = minSum;
        }

        count++;
        currentATMBalance -= sum;

        return sum;
    }

    public int putMoney(int sum) {
        int refund = 0;

        if(sum + currentATMBalance > maxATMBalance) {
            refund = sum + currentATMBalance - maxATMBalance;
        }

        count++;
        currentATMBalance += sum - refund;

        return refund;
    }

    public int getCount() {
        return count;
    }

    public String toString() {
        return "Current balance: " + currentATMBalance + "\n" + 
                "Maximum issuance: " + cashLimit + "\n" + 
                "Maximum possible ATM balance: " + maxATMBalance;
    }

}