public class Main {

    public static void main(String[] args) {        
        ATM atm = new ATM(50000, 10000, 100000);

        System.out.println(atm.getMoney(2000));
        System.out.println(atm.getMoney(20000));
        System.out.println(atm.putMoney(70000));
        System.out.println();

        System.out.println(atm);

        // ATM atm2 = new ATM(6000, 120000, 10000);
    }

}